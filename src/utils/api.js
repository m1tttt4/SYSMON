import openSocket from 'socket.io-client';
const commentProxy = 'https://' + window.location.hostname + ':10000';
console.log('Connecting to socket.io proxy: ', commentProxy);
const socket = openSocket(commentProxy);
// cb is the parameters passed to the api call
// err in null by default
var receiving = {};
function subscribeToTimer(cb) {
	socket.on('timer', timestamp => cb(null, timestamp));
	socket.emit('subscribeToTimer', 1000);
}
function saveComment(cb) {
	socket.emit('saveComment', cb);
}
//Returns all comments for given key
function subscribeToComments(key, cb) {
	socket.emit('subscribeToComments', key)
  receiving[key] = setInterval(() => {
    socket.emit('subscribeToComments', key)
  }, 1000)
  console.log('key: ', key)
  if (!socket.hasListeners(key)){
    console.log('cb: ',cb)
    socket.on(key,
        (console.log('receiving comments for key: ', key),
        comments => cb(null, comments)))
  }
}
function unsubscribeToComments(key) {
	socket.emit('unsubscribeToComments', key)
  clearInterval(receiving[key])
}

function deleteComment(key, cb) {
	socket.emit('deleteComment', key);
  
  if (!socket.hasListeners('comment_deleted')){
    console.log('cb: ',cb)
    socket.on('comment_deleted',
        comment_deleted => cb(null, comment_deleted))
  }
}

export { 
  subscribeToTimer,
  subscribeToComments,
  unsubscribeToComments,
  saveComment,
  deleteComment
};
