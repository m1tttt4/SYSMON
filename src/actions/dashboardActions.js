import * as types from '../constants/ActionTypes';

export function setState(settings, fieldName, value) {
	
	return { type: types.SET_STATE, settings, fieldName, value };
}
export function setStates(settings, fieldNames, values) {
	
	return { type: types.SET_STATES, settings, fieldNames, values };
}
export function append(settings, fieldName, value) {
	
	return { type: types.APPEND, settings, fieldName, value };
}
export function SEARCH(settings, fieldName, value) {
	
	return { type: types.SEARCH, settings, fieldName, value };
}



 