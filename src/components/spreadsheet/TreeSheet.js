import React, { Component } from 'react';
import createFragment from 'react-addons-create-fragment';
import ReactDataGrid from 'react-data-grid';
import update from 'immutability-helper';
import { Editors, Formatters, Toolbar, Filters, Data, Draggable } from 'react-data-grid';
import Snackbar from 'material-ui/Snackbar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Checkbox from './Checkbox.js';
// import CommentList from './CommentList.js';
import Clock from './Clock.js';
import { saveComment, refreshComments, deleteComment, solarwinds } from '../../utils/api.js';
import LogWindow from './LogWindow.js';
import Icon from './Icon.js';


//const { AutoCompleteFilter, MultiSelectFilter, SingleSelectFilter } = Filters;
const { Selectors } = Data;
const DraggableContainer = Draggable.Container;
const { AutoComplete: AutoCompleteEditor, DropDownEditor } = Editors;
const { DropDownFormatter } = Formatters;

class EmptyRowsView extends Component {
  render() {
    return (<div>dropdown</div>)
  }
};

class TreeSheet extends Component {
  constructor(props) {
    super(props);
    activeColumnHeaders: '';
    devices: '';
    data: '';
    this.state = {
      selectedIndexes: [], 
      gridHeight: window.innerHeight - 150, 
      gridWidth: window.innerWidth, 
      columns: [],
      rows: [],
      filters: [],
      timestamp: 'no timestamp yet',
      logWidth: '0',
      logDisplay: 'none',
      comments: '',
      expanded: {},
      snackbarOpen: false,
      snackbarMessage: 'Comment Added',
      snackbarDuration: 4000
    }
  };
  componentWillReceiveProps(nextProps){
    //clear filters if not all column headers are present in next iteration
    if (this.props.allColumnHeaders.length !== nextProps.allColumnHeaders.length || 
        this.props.allColumnHeaders.every((v,i) => v !== nextProps.allColumnHeaders[i])) { 
      this.onClearFilters(); 
    }
    if (JSON.stringify(this.props.comments) !== JSON.stringify(nextProps.comments) 
        || this.props.environ !== nextProps.environ 
        || this.props.allColumnHeaders !== nextProps.allColumnHeaders 
        || this.props.activeColumnHeaders !== nextProps.activeColumnHeaders) {
      const newColumns = this.createColumns(nextProps.activeColumnHeaders,
                                            nextProps.keys);
      const rowData = this.createRows(nextProps.activeColumnHeaders, 
                                      nextProps.comments,
                                      nextProps.environ,
                                      nextProps.devices,
                                      nextProps.data,
                                      nextProps.keys);
      const newRows = rowData[0];
      const expanded = rowData[1];
      this.setState({ 
        columns: newColumns, 
        rows: newRows,
        expanded: expanded,
        selectedIndexes: []
      })
    }
  };
  shouldComponentUpdate(nextProps, nextState) {
    let idkshouldit = this.state.filters !== nextState.filters 
        || this.props.environ !== nextProps.environ 
        || this.state !== nextState 
    return idkshouldit;
  };
  componentWillUpdate(nextProps, nextState) {
  };
  componentDidUpdate(prevProps, prevState) {
    /* console.log("TreeSheet Updated") */
  };
  componentWillMount() {
    this.keyCombos = new Set();
    this.selectedCheckboxes = new Set();
  };
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions.bind(null, this)); 
  };
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(null, this)); 
  };
  updateDimensions = () => {
    let update_height = window.innerHeight - 150;
    let update_width  = window.innerWidth;
    this.setState({ gridHeight: update_height, gridWidth: update_width});
  };

  // Called by componentWillReceiveProps
  //    activeColumnHeaders: prop from ColumnSelector
  //    allComments: prop from ColumnSelector
  //    environ: prop from ColumnSelector - user selected environ
  //    devices: prop from ColumnSelector - all json devices for given environ
  //    data: prop from ColumnSelector - all json logs for given environ
  //    keys: prop from ColumnSelector - indices used for creating rows
  // Returns array of row objects and expanded
  createRows(activeColumnHeaders, allComments, environ, devices, data, keys) {
    /* console.log('CreatingRows') */
    let rows = [];
    let key1 = keys[0], key1value
    let key2 = keys[1], key2value
    if (activeColumnHeaders.length > 0){
      for (let i = 0; i < (devices.length); i++) {
        key1value = devices[i][key1]
        key2value = devices[i][key2]
        let rowEntry = {
          id: key1value,
          [key1]: key1value,
          [key2]: key2value,
          comments: '',
          children: []
        }
        let commentCount = 0;
        for (let j=0; j < allComments.length; j++) {
          if(!allComments[j]['key']) {
            break;
          }
          let comment;
          if (allComments[j]['key'] == key1value) {
            try { 
              comment = allComments[j]['text'].join('\n')
            } catch (TypeError) {
              comment = allComments[j]['text']
            }
            rowEntry['children'].push({
              id: key1value, 
              commentId: allComments[j]['_id'], 
              [key1]: key1value, 
              [key2]: key2value, 
              comments: comment
            }); 
            commentCount++;
          }
          if (commentCount == 0) {
            continue;
          } else if (commentCount == 1) {
            rowEntry['comments'] = commentCount + ' comment';
          } else {
            rowEntry['comments'] = commentCount + ' comments';
          }
        }
        for (let j=0; j < data.length; j++) {
          if(!data[j][key1]) {
            break;
          }
          if (data[j][key1] == key1value || data[j][key1] === key2value) {
            for (let key in activeColumnHeaders) {
              if (data[j][activeColumnHeaders[key]] && !rowEntry[activeColumnHeaders[key]]) {
                try { 
                  rowEntry[activeColumnHeaders[key]] = data[j][activeColumnHeaders[key]].join('\n'); 
                } catch (TypeError) { 
                  rowEntry[activeColumnHeaders[key]] = data[j][activeColumnHeaders[key]]; 
                }
                for (let child in rowEntry['children']){
                  rowEntry['children'][child][activeColumnHeaders[key]] = ''
                }
              }
            }
          } 
        }
        rows.push(rowEntry)
      } 
    } 
    let _expanded = Object.assign({}, this.state.expanded);
    for (let i = 0; i < rows.length; i++) {
        let rowKey = rows[i].id;
        let rowIndex = rows.indexOf(rows[i]);
        let subRows = rows[i].children;
        if (subRows && _expanded[rowKey] !== false) {
          _expanded[rowKey] = true;
          this.updateSubRowDetails(subRows, rows[i].treeDepth);
          rows.splice(rowIndex + 1, 0, ...subRows);
        } else {
        }
    }
    /* console.log('End of createRows') */
    return [rows, _expanded];
  };
  createColumns(activeColumnHeaders, keys) {
    let _columns = [];
    if (activeColumnHeaders.length > 0){
      for (let i = 0; i < activeColumnHeaders.length; i++) {
        let newColumn = []
        if( activeColumnHeaders[i].split(',').length > 1 ){
          newColumn = {
            key: activeColumnHeaders[i],
            name: activeColumnHeaders[i].split(',')[1],
            filterable: true,
            width: 150,
            draggable: false,
            resizable: true,
            sortable: true
          };
        } else {
          newColumn = {
            key: activeColumnHeaders[i],
            name: activeColumnHeaders[i],
            filterable: true,
            width: 150,
            draggable: false,
            resizable: true,
            sortable: true
          };
        }
        _columns.push(newColumn)
      }
      _columns.push({
        key: 'comments',
        name: 'comments',
        width: 200,
        filterable: true,
        sortable: true,
        draggable: false,
        editable: true
      })
    }
    return _columns 
  };
  onCellExpand(args) {
    /* console.log(args) */
    let rows = this.state.rows.slice(0);
    let rowKey = args.rowData.id;
    let rowIndex = rows.indexOf(args.rowData);
    let subRows = args.expandArgs.children;
    let expanded = Object.assign({}, this.state.expanded);
    if (expanded[rowKey] == false) {
      expanded[rowKey] = true;
      this.updateSubRowDetails(subRows, args.rowData.treeDepth);
      rows.splice(rowIndex + 1, 0, ...subRows);
    } else if (expanded[rowKey] == true) {
      expanded[rowKey] = false;
      this.updateSubRowDetails(subRows, args.rowData.treeDepth);
      rows.splice(rowIndex + 1, subRows.length);
    }
    this.setState({ expanded: expanded, rows: rows });
  };
  collapseComments() {
    /* console.log(this.state) */
    let expanded = Object.assign({}, this.state.expanded);
    for (let e in expanded) {
      if (expanded[e]) {
        expanded[e] = !expanded[e]
      }
    }
    this.setState({
      expanded: expanded
    })
  };
  getSubRowDetails(rowItem) {
    let isExpanded = this.state.expanded[rowItem.id] ? this.state.expanded[rowItem.id] : false;
    return {
      group: rowItem.children && rowItem.children.length > 0,
      expanded: isExpanded,
      children: rowItem.children,
      field: 'comments',
      treeDepth: rowItem.treeDepth || 0,
      siblingIndex: rowItem.siblingIndex,
      numberSiblings: rowItem.numberSiblings
    };
  };
  updateSubRowDetails(subRows, parentTreeDepth) {
    let treeDepth = parentTreeDepth || 0;
    subRows.forEach((sr, i) => {
      sr.treeDepth = treeDepth + 1;
      sr.siblingIndex = i;
      sr.numberSiblings = subRows.length;
    });
  };
  handleActionClick() {
    this.setState({
      snackbarOpen: false,
    });
  };
  handleChangeDuration(event) {
    const value = event.target.value;
    this.setState({
      snackbarDuration: value.length > 0 ? parseInt(value) : 0,
    });
  };
  handleRequestClose() {
    this.setState({
      snackbarOpen: false,
    });
  };
  onDeleteSubRow(args) {
    // let idToDelete = args.rowData.id;
    // let rows = this.state.rows.slice(0);
    deleteComment(args.rowData.commentId,  (err, commentId) => (
      console.log('Deleted comment with id: ', commentId)
    ))
    this.setState({
      snackbarOpen: true,
      snackbarMessage: 'Comment Deleted'
    })
  };
  getRows() {
    return Selectors.getRows(this.state);
  };
  getSize() {
    return this.getRows().length;
  };
  invertFilter() {
    this.setState({rows: []})
  };
  handleGridSort(sortColumn, sortDirection) {
    this.setState({ sortDirection: sortDirection, sortColumn: sortColumn });
  };
  onHeaderDrop(source, target) {
    const stateCopy = Object.assign({}, this.state);
    const columnSourceIndex = this.state.columns.findIndex( i => i.key === source );
    const columnTargetIndex = this.state.columns.findIndex( i => i.key === target );

    stateCopy.columns.splice(columnTargetIndex, 0, stateCopy.columns.splice(columnSourceIndex, 1)[0]);

    const emptyColumns = Object.assign({},this.state, { columns: [] });
    this.setState( emptyColumns );
    const reorderedColumns = Object.assign({},this.state, { columns: stateCopy.columns });
    this.setState( reorderedColumns );
  };
  onClearFilters() {
    this.setState({filters: {} });
  };
  onKeyDown(e) {
    if (e.ctrlKey && e.keyCode === 65) {
      e.preventDefault();
      let rows = [];
      this.state.rows.forEach((r) =>{ rows.push(Object.assign({}, r, {isSelected: true})); });
      this.setState({ rows });
    }
  };
  onRowsSelected(rows) { 
    this.setState({selectedIndexes: this.state.selectedIndexes.concat(rows.map(r => r.rowIdx))}); 
    /* console.log(rows[0]) */
  };
  onRowsDeselected(rows) {
    let rowIndexes = rows.map(r => r.rowIdx);
    this.setState({selectedIndexes: this.state.selectedIndexes.filter(i => rowIndexes.indexOf(i) === -1 )});
  };
  createHeader() {
    return <div style={{position: 'absolute', marginTop: '5px'}}>
        {this.downloadButton()} {this.collapseButton()}
      </div>;
  }
  collapseButton() {
    return <RaisedButton fullWidth={false} onClick={() => {this.collapseComments();}}>
          Collapse all comments
        </RaisedButton>
  }
  downloadButton(){
    let csvContent = "data:text/csv;charset=utf-8,"
    let activeRows = Selectors.getRows(this.state);
    let activeHeaders
    let tmpHeaders
    try {
      activeHeaders = Object.keys(activeRows[0]);
      tmpHeaders = activeHeaders.map(column => column.replace(/,/gm,"_"));
      const replacer = (key, value) => (typeof(value) === 'string' ? value.replace(/,/gm,"_COMMA_").replace(/#/gm,"_HASH_").replace(/(\r\n\t|\r\t|\n)/gm," ") : '');
      let rows = activeRows.map(row => activeHeaders.map(column => JSON.stringify(row[column], replacer)).join(','));
      rows.unshift(tmpHeaders.join(','));
      csvContent += '\r\n' + rows.join(",\r\n")
      return <RaisedButton href={encodeURI(csvContent)} download="selection.csv" fullWidth={false}>
                Download Selected
              </RaisedButton>
    } catch (TypeError) {
      return 
    }
  };
  logWindowStyle() {
    return {
      width: window.innerWidth * this.state.logWidth,
      overflowX: 'none',
      overflowY: 'none', 
      right: '0',
      bottom: '3%',
      height: this.state.gridHeight ,
      whiteSpace: 'pre-wrap', 
      position: 'absolute', 
      textAlign: 'left',
      float: 'right',
      padding: '3%',
      background: 'white',
      borderRadius: '0px 0px 0px 10px',
      display: this.state.logDisplay
    }
  };
  toggleLogWindow() {
    if (this.state.logWidth > 0) { this.setState({logWidth: '0', logDisplay: 'none'})   }
    else { this.setState({logWidth: '.3', logDisplay: 'inline-block'}) }
  };
  growLogWindow = () => {
    this.setState({
      logWidth: parseFloat(this.state.logWidth) + .2
    })
  };
  shrinkLogWindow = () => {
    if (this.state.logWidth == '.3'){
      this.setState({
        logWidth: '0',
        logDisplay: 'none'
      })
    } else {
      this.setState({
        logWidth: parseFloat(this.state.logWidth) - .2
      })
    }
  };
  handleFilterChange(filter) {
    let newFilters = Object.assign({}, this.state.filters);
    if (filter.filterTerm) { 
      newFilters[filter.column.key] = filter; 
    }
    else { 
      delete newFilters[filter.column.key]; 
    }
    this.setState({ filters: newFilters });
  };
  handleGridRowsUpdated({ fromRow, toRow, updated }) {
    let rows = this.state.rows.slice();
    // console.log("handling grid row update")
    for (let i = fromRow; i <= toRow; i++) {
      let rowToUpdate = rows[i];
      let updatedRow = update(rowToUpdate, {$merge: updated});
      rows[i] = updatedRow;
      if (rows[i].comments == ''){
        this.setState({
          snackbarOpen: true,
          snackbarMessage: 'Comment must not be empty',
          snackbarDuration: 6000
        })
        break;
      }
      let comment = {
        author: 'Matthew',
        text: rows[i].comments,
        date: new Date(),
        key: rows[i].id,
        environ: this.props.environ
      }
      if (rows[i].commentId) {
        deleteComment(rows[i].commentId, (err, commentId) => {
          console.log('Deleted comment with id: ', commentId)
        })
        saveComment(comment)
        this.setState({
          snackbarOpen: true,
          snackbarMessage: 'Replaced comment'

        })
      } else {
        saveComment(comment)
        this.setState({
          snackbarOpen: true,
          snackbarMessage: 'Comment Added'
        })
      }
    }
  };
  rowGetter(rowIdx) {
    let rows = this.getRows();
    return rows[rowIdx];
  };
  onCellSelected = (selected) => {
    // console.log(this.rowGetter(selected.rowIdx)[this.state.columns[selected.idx-1]['key']])
    //If selected cell has children ( comments ) return //##display all in activeCellValue
    if (selected.idx < 3 
        || ( this.rowGetter(selected.rowIdx).children 
            && this.state.columns[selected.idx-1].name == "comments"
          )
        ) { 
      return 
    } 
    else { 
      this.setState({ 
        activeCellValue: this.rowGetter(selected.rowIdx)[this.state.columns[selected.idx-1]['key']]
      })
    }
    if (parseFloat(this.state.logWidth) == 0){
      this.setState({
        logWidth: '.3',
        logDisplay: 'inline-block'
      })
    }
  };
  render() {
    return (<div className='TreeSheetWrap' style={{height: '100%', width: '100%'}}>
      <div className='logWindow' style={this.logWindowStyle()}>
        <div className='growLog' style={{ top: '50%', cursor: 'pointer', position: 'absolute', width: '10px', left: '10px'}}>
          <div onClick={this.growLogWindow}>
            <Icon size="2rem" icon="left_chevron" viewBox="450 0 2300 2300" />
          </div>
          <div onClick={this.shrinkLogWindow}>
            <Icon size="2rem" icon="right_chevron" viewBox="450 0 2300 2300" />
          </div>
        </div>
        <div> {this.state.selectedIndexes.length} </div>
        <Paper style={{height: '100%', width: '100%', textAlign: 'left'}} zDepth={5}>
          <div className='log' style={{
              height: '100%', 
              width: '100%', 
              top: '50px',
              textAlign: 'left',
              overflowX: 'scroll',
              overflowY: 'scroll',
              marginLeft: '1px',
              }}>
            {this.state.activeCellValue}
          </div>
        </Paper>
        <div style={{textAlign: 'center', marginTop: '5px'}}> 
          <RaisedButton fullWidth={true} onClick={() => {this.toggleLogWindow();}}>
            Hide Log 
          </RaisedButton>
        </div>
      </div>
      {this.createHeader()}
      <DraggableContainer 
          onHeaderDrop={this.onHeaderDrop}
          minWidth={this.state.gridWidth * (1-this.state.logWidth)}
          height={this.state.gridHeight}
          environ={this.props.environ}
          deviceType={this.props.deviceType}>
        <ReactDataGrid
            ref={ node => this.grid = node }
            columns={this.state.columns}
            rowGetter={this.rowGetter.bind(this)}
            rowHeight={35}
            enableCellSelect={true}
            rowsCount={this.getSize()}
            minHeight={this.state.gridHeight}
            minWidth={this.state.gridWidth * (1-this.state.logWidth)}
            toolbar={<Toolbar enableFilter={true}/>}
            onGridSort={this.handleGridSort.bind(this)}
            onAddFilter={this.handleFilterChange.bind(this)}
            onClearFilters={this.onClearFilters.bind(this)} 
            onCellSelected={this.onCellSelected} 
            onGridKeyDown={this.onKeyDown}
            emptyRowsView={EmptyRowsView}
            onGridRowsUpdated={this.handleGridRowsUpdated.bind(this)}
            getSubRowDetails={this.getSubRowDetails.bind(this)}
            onDeleteSubRow={this.onDeleteSubRow.bind(this)}
            onCellExpand={this.onCellExpand.bind(this)}
            enableRowSelect={null}
            rowScrollTimeout={null}
            rowSelection={{
              showCheckbox: true,
              enableShiftSelect: true,
              selectBy: {
                indexes: this.state.selectedIndexes
              },
              onRowsSelected: this.onRowsSelected.bind(this),
              onRowsDeselected: this.onRowsDeselected.bind(this)
            }}/>
      </DraggableContainer>
      <Snackbar
          open={this.state.snackbarOpen}
          message={this.state.snackbarMessage}
          autoHideDuration={this.state.snackbarDuration}
          onActionClick={this.handleActionClick.bind(this)}
          onRequestClose={this.handleRequestClose.bind(this)}/>
    </div>);
  }
};

export default TreeSheet;
