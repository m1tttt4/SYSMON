import React from 'react';
import Paper from 'material-ui/Paper';

const logWindowStyle = {
		marginLeft: '4px',
		height: '100%'
	}

class LogWindow extends React.Component {

	toggleLogWindow() {
		if (this.props.logWidth > 0){
			this.props.setState({}, 'logWidth', '0')
		}else{
			this.props.setState({}, 'logWidth', '.3')
		}
	};

	render() {
		// console.log('                                           LogWindow Render')
		return (
			<Paper style={{height: '200px', width: '200px', textAlign: 'center'}} zDepth={3}>
				<div className='log'>{this.props.activeCellValue}</div>
			</Paper>
		);
	}
}

export default LogWindow;