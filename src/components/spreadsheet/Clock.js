import React, { Component, PropTypes } from 'react';
import { subscribeToTimer } from '../../utils/api.js';


class Clock extends Component {
	constructor(props) {
		super(props);
		this.state = {
			timestamp: 'no timestamp yet'
		};
	}

	componentDidMount() {
		subscribeToTimer((err, timestamp) => this.setState({ 
			timestamp 
		}));
	}


	render() {
		return (
			<div>
				Timestamp: {this.state.timestamp}
			</div>
		);
	}

}

export default Clock;