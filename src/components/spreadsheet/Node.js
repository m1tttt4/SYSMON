import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

const rotateBox = 'rotate(-45deg)';

const rowStyle = {
	display: 'flex',
	position: 'relative',
	width: '200px',
	cursor: 'pointer'
}
const labelStyle = {
	width: '200px',
	height: '100%',
	display: 'inline-flex',
	cursor: 'pointer',
}
class Node extends Component {
	state = { isChecked: false }
	toggleNodeChange = () => {
		const { handleNodeChange, label } = this.props;
		handleNodeChange(label);
	}

	render() {
		const { label, identifier } = this.props;
		let { isChecked } = this.state;
		if (this.props.selectedNodees) {
			for (let i in Array.from(this.props.selectedNodees)) {
				if (label == Array.from(this.props.selectedNodees)[i]){	isChecked = !isChecked; }
			}
		}
		return (
			<div className="boxRow" style={rowStyle}>
				<div style={{width: 'inherit'}}>
					<label style={labelStyle}>
						<input
							type="checkbox"
							value={label}
							checked={isChecked}
							onChange={this.toggleNodeChange}
							style={{position: 'absolute', width: '20px', float: 'left'}}
						/>
						<p style={{position: 'absolute', width: '100px', left: '20px', whiteSpace: 'pre'}}>{identifier}</p>
					</label>
				</div>
			</div>
		);
	}
}

// Node.propTypes = {
// 	label: PropTypes.string.isRequired,
// 	handleNodeChange: PropTypes.func.isRequired,
// };

export default Node;