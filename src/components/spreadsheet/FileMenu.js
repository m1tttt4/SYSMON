//NOT USED 03-12-2018
import React from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import Download from 'material-ui/svg-icons/file/file-download';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

/**
* Example of nested menus within an IconMenu.
*/
const FileMenu = () => (
	<IconMenu
		iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
		anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
		targetOrigin={{horizontal: 'left', vertical: 'top'}}
	>
		<MenuItem
			primaryText="Servers"
			rightIcon={<ArrowDropRight />}
			menuItems={[
				<MenuItem primaryText="NY4" />,
				<MenuItem primaryText="CH4" />,
				<Divider />,
				<MenuItem primaryText="All" />,
			]}
		/>

		<MenuItem
			primaryText="Case Tools"
			rightIcon={<ArrowDropRight />}
			menuItems={[
				<MenuItem primaryText="UPPERCASE" />,
				<MenuItem primaryText="lowercase" />,
				<MenuItem primaryText="CamelCase" />,
				<MenuItem primaryText="Propercase" />,
			]}
		/>
		<Divider />
		<MenuItem primaryText="Download" leftIcon={<Download />} />
		<Divider />
		<MenuItem value="Del" primaryText="Delete" />

	</IconMenu>
);

export default FileMenu;