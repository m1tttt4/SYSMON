import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

const rotateBox = 'rotate(-45deg)';

const rowStyle = {
	display: 'flex',
	position: 'relative',
	width: '200px',
	cursor: 'pointer'
}
const labelStyle = {
	width: '200px',
	height: '100%',
	display: 'inline-flex',
	cursor: 'pointer',
}
class Checkbox extends Component {
	state = { isChecked: false }
	toggleCheckboxChange = () => {
		const { handleCheckboxChange, label } = this.props;
		handleCheckboxChange(label);
	}

	render() {
		const { label, identifier } = this.props;
		let { isChecked } = this.state;
		if (this.props.selectedCheckboxes) {
			for (let i in Array.from(this.props.selectedCheckboxes)) {
				if (label == Array.from(this.props.selectedCheckboxes)[i]){	isChecked = !isChecked; }
			}
		}
		return (
			<div className="boxRow" style={rowStyle}>
				<div style={{width: 'inherit'}}>
					<label style={labelStyle}>
						<input
							type="checkbox"
							value={label}
							checked={isChecked}
							onChange={this.toggleCheckboxChange}
							style={{position: 'absolute', width: '20px', float: 'left'}}
						/>
						<p style={{position: 'absolute', width: '100px', left: '20px', whiteSpace: 'pre'}}>{identifier}</p>
					</label>
				</div>
			</div>
		);
	}
}

// Checkbox.propTypes = {
// 	label: PropTypes.string.isRequired,
// 	handleCheckboxChange: PropTypes.func.isRequired,
// };

export default Checkbox;