import React from 'react';
import { saveComment } from '../../utils/api.js';


class CommentForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			commentInput: ''
		};
		this.saveForm = this.saveForm.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	saveForm(event) {
		event.preventDefault();
		let comment = {
			author: 'Matthew',
			text: this.state.commentInput,
			date: Date(),
			key: this.props.dataKey,
			environ: this.props.environ
		}
		// console.log("Saving comment")
		console.log(comment)
		saveComment(comment)
	}
	handleChange(event) {
		this.setState({commentInput: event.target.value});
	}


	render() {
		return (
			<form onSubmit={this.saveForm}>
				<label>
					<textarea style={{height: '24px', border: 'none'}} value={this.state.commentInput} onChange={this.handleChange} />
				</label>
				<input type="submit" value="Submit" />
			</form>
		);
	}
}

export default CommentForm;