import stackoverflowFixturesRaw from '../../../static/json/inventory/stackoverflow-survey.json';

const stackoverflowFixtures = JSON.parse(stackoverflowFixturesRaw);
console.log(stackoverflowFixtures);

export const d3actPieExtractMostPopularTechnologiesByYear = (year) => stackoverflowFixtures.mostPopularTechnologies[year].reduce((accumulator, current) => {
  accumulator[current.name] = (current.score * 100).toPrecision(4);// 0.544 * 100 = 54.400000000000006 :(
  return accumulator;
}, {});

export const d3actBarExtractMostPopularTechnologiesByYear = (year) => stackoverflowFixtures.mostPopularTechnologies[year].reduce((accumulator, current) => {
  accumulator.push({
    xValue: current.name,
    yValue: parseFloat((current.score * 100).toPrecision(4))// 0.544 * 100 = 54.400000000000006 :(
  });
  return accumulator;
}, []);

export const d3actPieExtractDesktopOperatingSystemByYear = (year) => stackoverflowFixtures.desktopOperatingSystem[year].reduce((accumulator, current) => {
  accumulator[current.name] = (current.score * 100).toPrecision(4);// 0.544 * 100 = 54.400000000000006 :(
  return accumulator;
}, {});

export const d3actBarExtractDesktopOperatingSystemByYear = (year) => stackoverflowFixtures.desktopOperatingSystem[year].reduce((accumulator, current) => {
  accumulator.push({
    xValue: current.name,
    yValue: parseFloat((current.score * 100).toPrecision(4))// 0.544 * 100 = 54.400000000000006 :(
  });
  return accumulator;
}, []);

