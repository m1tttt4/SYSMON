import React, { Component } from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import createFragment from 'react-addons-create-fragment';
import { subscribeToComments, unsubscribeToComments } from '../../utils/api';
import Checkbox from './Checkbox';
import Icon from './Icon';
import TreeSheet from './TreeSheet';
import json_install_base_A from '../../../static/json/inventory/install_base_A.json';
import json_install_base_Z from '../../../static/json/inventory/install_base_Z.json';
import json_datastores from '../../../static/json/inventory/vmsToDatastores.json';
import json_phys_linux from '../../../static/json/inventory/all_phys_linux.json';
import json_all_switches from '../../../static/json/inventory/all_switches.json';
import json_all_vms from '../../../static/json/inventory/all_vms.json';
// data
import json_all_server_logs from '../../../static/json/logs/all_server_logs.json';
import json_all_switch_logs from '../../../static/json/logs/all_switch_logs.json';
import json_all_vm_logs from '../../../static/json/logs/all_vm_logs.json';

// import styles from './css/spreadsheet.css';
const menuStyle = {
  height: '5em',
  position: 'relative',
  overflow: 'hidden',
  width: '100%',
  display: 'inline-flex',
};

const selectorIconStyle = {};

const envContainerStyle = {
  borderColor: '#e7eaec',
  borderWidth: '1px',
  borderStyle: 'solid solid none',
  position: 'absolute',
  display: 'inline-flex',
  overflowX: 'hidden',
  overflowY: 'hidden',
  width: '50%',
  minWidth: '340px',
  textAlign: 'right',
};
const checkboxContainerStyle = {
  borderColor: '#e7eaec',
  borderWidth: '1px',
  borderStyle: 'solid solid none',
  right: '0',
  position: 'absolute',
  display: 'inline-flex',
  overflowX: 'hidden',
  overflowY: 'hidden',
  width: '50%',
  minWidth: '340px',
  textAlign: 'right',
};

class ColumnSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sheetData: [],
      sheetDevices: [],
      allColumnHeaders: [],
      activeColumnHeaders: [],
      keys: [],
      comments: [],
      environ: '',
      deviceType: '',
    };
    this.addedHardwareCheckBoxes = [];
    this.addedOSCheckBoxes = [];
    this.addedConfigCheckBoxes = [];
    this.addedOtherCheckBoxes = [];
    this.addedCheckBoxes = [];
  }
  compareArraysByProp(a1, a2, prop) {
    if (a1.length !== a2.length) { return false; }
    for (let i in a1) {
      if (!a2[i]) { return false }
      else {
        if (a1[i][prop] !== a2[i][prop]) { return false }
      }
    }
    return true;
  };
  shouldComponentUpdate(nextProps, nextState) {
    let idkshouldit = this.state.activeColumnHeaders !== nextState.activeColumnHeaders
        || this.state.environ !== nextState.environ
        || this.props !== nextProps
        || this.state.allColumnHeaders !== nextState.allColumnHeaders
        || this.state.keys !== nextState.keys
        || !this.compareArraysByProp(this.state.comments, nextState.comments, 'text')
    return idkshouldit
  };
  componentWillUpdate(nextProps, nextState) {
    this.addedHardwareCheckBoxes = []
    this.addedOSCheckBoxes = []
    this.addedConfigCheckBoxes = []
    this.addedOtherCheckBoxes = []
    this.addedCheckBoxes = []
    if (this.state.activeColumnHeaders !== nextState.activeColumnHeaders){
      this.selectedCheckboxes = new Set();
      this.labels = new Set()
    }
    for(let i=0; i<nextState.activeColumnHeaders.length; i++){
      this.selectedCheckboxes.add(nextState.activeColumnHeaders[i])
    }
  }
  componentDidUpdate(prevProps, prevState) {
    // console.log("ColumnSelector Updated")
  };
  componentWillReceiveProps(nextProps) {
  };

  componentWillUnmount() {
    unsubscribeToComments(this.state.environ);
  };

  componentWillMount() {
    this.selectedCheckboxes = new Set();
    this.labels = new Set()
  };
  componentDidMount() {
  };
  setTableType(environ, deviceType) {
    if (this.state.environ == environ && this.state.deviceType == deviceType) {
      return
    }
    let deviceList = [], dataList = [], allHeaderList = [], activeHeaderList = [], key1, key2;
    const allHeaders = new Set();
    const activeHeaders = new Set();
    switch(deviceType){
      case 'linux':
        dataList = json_all_server_logs;
        deviceList = json_phys_linux;
        key1 = "hostname";
        key2 = "ip";
        break;
      case 'server':
        dataList = json_all_server_logs;
        key1 = "hostname";
        key2 = "ip";
        switch(environ){
          case 'ALL': deviceList = json_all_servers; break;
          default: console.log('environ not specified')
        } break;
      case 'switch':
        dataList = json_all_switch_logs;
        key1 = "hostname";
        key2 = "ip";
        switch(environ){
          case 'ALL': deviceList = json_all_switches; break;
          default: console.log('environ not specified')
        } break;
      case 'vm-config':
        dataList = [];
        deviceList = json_all_vms;
        key1 = "hostname";
        key2 = "ip";
        break;
      case 'vm-linux':
        // Pulls keys from first json object and
        // Sets first to keys active. These should be hostname and ip.
        dataList = json_all_vm_logs;
        deviceList = []
        for (let device in json_all_vms){
          if (json_all_vms[device]['Guest OS'].toLowerCase().indexOf('centos') > -1
              || json_all_vms[device]['Guest OS'].toLowerCase().indexOf('linux') > -1) {
            deviceList.push(json_all_vms[device])
          }
        }
        key1 = "hostname";
        key2 = "ip";
        break;
      case 'install_base_A':
        dataList = json_install_base_A;
        deviceList = []
        for (let device in dataList){
          deviceList.push(dataList[device])
        }
        key1 = "Serial Number";
        key2 = "Z-Side Customer";
        break;
      case 'install_base_Z':
        dataList = json_install_base_Z;
        deviceList = []
        for (let device in dataList){
          deviceList.push(dataList[device])
        }
        key1 = "Serial Number";
        key2 = "A-Side Customer";
        break;
      case 'datastores':
        dataList = json_datastores;
        deviceList = []
        for (let device in dataList){
          deviceList.push(dataList[device])
        }
        key1 = "hostname";
        key2 = "vm.host.name";
        break;
      default:
        console.log('deviceType not specified')
    }
    activeHeaders.add(key1);
    activeHeaders.add(key2);
    this.selectedCheckboxes.add(key1)
    this.selectedCheckboxes.add(key2)
    // Add all found keys in device file to the dataList
    // then all keys from dataList to allHeaders
    for (let i=0; i<deviceList.length; i++) {
      dataList.push(deviceList[i])
    }
    for (let i=0; i<dataList.length; i++) {
      for (let key in Object.keys(dataList[i])) {
        allHeaders.add(Object.keys(dataList[i])[key])
      }
    }
    for (let i=0; i<Array.from(this.selectedCheckboxes).length; i++) {
      if (allHeaders.has(Array.from(this.selectedCheckboxes)[i])) {
        activeHeaders.add(Array.from(this.selectedCheckboxes)[i])
      }
    }
    allHeaderList = Array.from(allHeaders);
    activeHeaderList = Array.from(activeHeaders);
    if (this.state.environ != environ) {
      console.log("unsubscribing from " + this.state.environ)
      unsubscribeToComments(this.state.environ)
      console.log("subscribing to " + environ)
      subscribeToComments(environ, (err,comments) => (
        this.setState({
          ...comments
        })
      ));
    }
    this.setState({
      deviceType: deviceType,
      activeColumnHeaders: activeHeaderList,
      sheetData: dataList,
      sheetDevices: deviceList,
      allColumnHeaders: allHeaderList,
      keys: [key1, key2],
      environ: environ
    })
  };
  handleCheckboxChange = label => {
    if (this.selectedCheckboxes.has(label)) {
      this.selectedCheckboxes.delete(label);
    }
    else {
      this.selectedCheckboxes.add(label);
    }
    let nextColumns = Array.from(this.selectedCheckboxes)
    this.setState({activeColumnHeaders: nextColumns})
  };
  createConfigCheckbox = label => {
    if(label.split(',')[0] == 'configs'
        && !this.addedCheckBoxes.includes(label)
        && !this.addedConfigCheckBoxes.includes(label)) {
      this.addedConfigCheckBoxes.push(label)
      this.addedCheckBoxes.push(label)
      return <Checkbox label={label}
            handleCheckboxChange={this.handleCheckboxChange}
            key={label}
            identifier={label.split(',')[1]}
            selectedCheckboxes={this.selectedCheckboxes} />
    }
  };
  createOSCheckbox = label => {
    if(label.split(',')[0] == 'os'
        && !this.addedCheckBoxes.includes(label)
        && !this.addedOSCheckBoxes.includes(label)) {
      this.addedOSCheckBoxes.push(label)
      this.addedCheckBoxes.push(label)
      return <Checkbox label={label}
            handleCheckboxChange={this.handleCheckboxChange}
            key={label}
            identifier={label.split(',')[1]}
            selectedCheckboxes={this.selectedCheckboxes} />
    }
  };
  createHardwareCheckbox = label => {
    if(label.split(',')[0] == 'hardware'
        && !this.addedCheckBoxes.includes(label)
        && !this.addedHardwareCheckBoxes.includes(label)) {
      this.addedHardwareCheckBoxes.push(label)
      this.addedCheckBoxes.push(label)
      return <Checkbox label={label}
            handleCheckboxChange={this.handleCheckboxChange}
            key={label}
            identifier={label.split(',')[1]}
            selectedCheckboxes={this.selectedCheckboxes} />
    }
  };
  createOtherCheckbox = label => {
    if(label.split(',')[0] !== 'configs'
        && label.split(',')[0] !== 'hardware'
        && label.split(',')[0] !== 'os'
        && !this.addedCheckBoxes.includes(label)
        && !this.addedOtherCheckBoxes.includes(label)) {
      this.addedOtherCheckBoxes.push(label)
      this.addedCheckBoxes.push(label)
      return <Checkbox label={label}
            handleCheckboxChange={this.handleCheckboxChange}
            key={label}
            identifier={label}
            selectedCheckboxes={this.selectedCheckboxes} />
    }
  };
  allConfigCheckboxes = () => (
    this.state.allColumnHeaders.sort().map(this.createConfigCheckbox)
  );
  allOSCheckboxes = () => (
    this.state.allColumnHeaders.sort().map(this.createOSCheckbox)
  );
  allHardwareCheckboxes = () => (
    this.state.allColumnHeaders.sort().map(this.createHardwareCheckbox)
  );
  allOtherCheckboxes = () => (
    this.state.allColumnHeaders.sort().map(this.createOtherCheckbox)
  );

  render() {
    return (<div>
      <div id='menu' style={menuStyle}>
        <div style={envContainerStyle}>
          <DropDownMenu value={0} anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} title="ALL">
            <MenuItem value={0} label={"Devices"} style={{display: 'none'}}></MenuItem>
            <MenuItem key="All.switch" onClick={() => { this.setTableType('ALL', 'switch');}}><Icon style={selectorIconStyle} size="2rem" icon="switch_icon" viewBox="0 0 24 24"/>Switches</MenuItem>
            <MenuItem key="All.linux" onClick={() => { this.setTableType('ALL', 'linux');}}><Icon style={selectorIconStyle} size="2rem" icon="server_icon" viewBox="0 0 24 24"/>Linux</MenuItem>
            <MenuItem key="All.vm-config" onClick={() => { this.setTableType('ALL', 'vm-config');}}><Icon style={selectorIconStyle} size="2rem" icon="server_icon" viewBox="0 0 24 24"/>VM Configs</MenuItem>
            <MenuItem key="All.vm-linux" onClick={() => { this.setTableType('ALL', 'vm-linux');}}><Icon style={selectorIconStyle} size="2rem" icon="server_icon" viewBox="0 0 24 24"/>VM Linux</MenuItem>
            <MenuItem key="All.datastores" onClick={() => { this.setTableType('ALL', 'datastores');}}><Icon style={selectorIconStyle} size="2rem" icon="server_icon" viewBox="0 0 24 24"/>Datastores</MenuItem>
          </DropDownMenu>

          <DropDownMenu value={0} anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} title="Install_Base">
            <MenuItem value={0} label={"Install_Base"} style={{display: 'none'}}></MenuItem>
            <MenuItem onClick={() => { this.setTableType('IB', 'install_base_A');}}><Icon style={selectorIconStyle} size="2rem" icon="server_icon" viewBox="0 0 24 24"/>A-Side</MenuItem>
            <MenuItem onClick={() => { this.setTableType('IB', 'install_base_Z');}}><Icon style={selectorIconStyle} size="2rem" icon="server_icon" viewBox="0 0 24 24"/>Z-Side</MenuItem>
          </DropDownMenu>
        </div>
        <div style={checkboxContainerStyle}>
          <DropDownMenu value={0} anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} maxHeight={500} style={{width: '25%', float: 'right'}}>
            {this.allConfigCheckboxes()}
            <MenuItem value={0} label={createFragment({left: 'Configs: ', right: this.addedConfigCheckBoxes.length})} style={{display: 'none'}}></MenuItem>
          </DropDownMenu>
          <DropDownMenu value={0} anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} maxHeight={500} style={{width: '25%', float: 'right'}}>
            {this.allHardwareCheckboxes()}
            <MenuItem value={0} label={createFragment({left: 'Hardware: ', right: this.addedHardwareCheckBoxes.length})} style={{display: 'none'}}></MenuItem>
          </DropDownMenu>
          <DropDownMenu value={0} anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} maxHeight={500} style={{width: '25%', float: 'right'}}>
            {this.allOSCheckboxes()}
            <MenuItem value={0} label={createFragment({left: 'OS: ', right: this.addedOSCheckBoxes.length})} style={{display: 'none'}}></MenuItem>
          </DropDownMenu>
          <DropDownMenu value={0} anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} maxHeight={500} style={{width: '25%', float: 'right'}}>
            {this.allOtherCheckboxes()}
            <MenuItem value={0} label={createFragment({left: 'Other: ', right: this.addedOtherCheckBoxes.length})} style={{display: 'none'}}></MenuItem>
          </DropDownMenu>
        </div>
      </div>
      <TreeSheet
        keys={this.state.keys}
        environ={this.state.environ}
        deviceType={this.state.deviceType}
        devices={this.state.sheetDevices}
        data={this.state.sheetData}
        allColumnHeaders={this.state.allColumnHeaders}
        activeColumnHeaders={this.state.activeColumnHeaders}
        setState={this.props.setState}
        setStates={this.props.setStates}
        appendState={this.props.append}
        comments={this.state.comments}
      />
    </div>);
  }
}

export default ColumnSelector;

