import React from 'react';
import Comment from './Comment.js';
import CommentForm from './CommentForm.js';


class CommentList extends React.Component {
	constructor(props) {
		super(props);
		this.comments = [];
		let dataKey = this.props.dataKey;
		let allComments = this.props.allComments;
		for (let j=0; j < allComments.length; j++) {
			if(!allComments[j]['key']) {
				break;
			}
			if (allComments[j]['key'] == dataKey) {
				console.log(allComments[j])
				try { 
					console.log('adding comment for ' + dataKey)
					this.comments.push(allComments[j]['text'].join('\n')); 
				}
				catch (TypeError) {	
					this.comments.push(allComments[j]['text']);	
				}
			}
		}
	}

	createComment = text => {
		return <Comment 
				key={text}
				text={text} />
	}
	createAllComments = () => (this.comments.map(this.createComment));
				
	render() {
		return (
			<div>
				{this.createAllComments()}<br/>
				addComment: <CommentForm
							style={{display: 'none'}}
							dataKey={this.props.dataKey}
							environ={this.props.environ}
							/>
			</div>
		);
	}
}

export default CommentList;