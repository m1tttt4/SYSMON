import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

const rotateBox = 'rotate(-45deg)';

const rowStyle = {
  display: 'flex',
  position: 'relative',
  width: '28px',
  transform: rotateBox,
  transformOrigin: '0% 0%',
}
const labelStyle = {
  width: '100%',
  height: '100%',
  display: 'inline-flex',
}
class Comment extends Component {

  render() {
    return (
      <label style={labelStyle}>
        <p style={{position: 'absolute', width: '100px', left: '20px', whiteSpace: 'pre'}}>{this.props.text}</p>
      </label>
    );
  }
}

// Comment.propTypes = {
//  text: PropTypes.string.isRequired
// };

export default Comment;