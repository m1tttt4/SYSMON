//CommentBox.js
import React, { Component } from 'react';
import CommentList from './list';
import CommentForm from './form';
import style from './style';
import axios from 'axios';


class CommentBox extends Component {
	constructor(props) {
		super(props);
		this.state = { data: [] };
		this.loadCommentsFromServer = this.loadCommentsFromServer.bind(this);
		this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
		this.handleCommentDelete = this.handleCommentDelete.bind(this);
		this.handleCommentUpdate = this.handleCommentUpdate.bind(this);
		this.sendTextToGraph = this.sendTextToGraph.bind(this);
	}
	loadCommentsFromServer() {
		axios.get(this.props.url)
		.then(res => {
			this.setState({ data: res.data });
			console.log(res)
		})
	}
	handleCommentSubmit(comment) {
		//add POST request
		let comments = this.state.data;
		comment.id = Date.now();
		let newComments = comments.concat([comment]);
		this.setState({ data: newComments });
		axios.post(this.props.url, comment)
			.catch(err => {
				console.error(err);
				this.setState({ data: comments });
			});
	}
	handleCommentDelete(id) {
		axios.delete(`${this.props.url}/${id}`)
		.then(res => {
			console.log('Comment deleted');
		})
		.catch(err => {
			console.error(err);
		});
	}

	handleCommentUpdate(id, comment) {
		//sends the comment id and new author/text to our api
		axios.put(`${this.props.url}/${id}`, comment)
		.catch(err => {
			console.log(err);
		});

		this.sendTextToGraph(id, comment.text)
	}
	sendTextToGraph(id, comment) {
		let text = comment ? comment : null;
		//brings up the update field when we click on the update link.
		if (text) {
			let textDataArray = text.split(',');
			for ( var i=0; i<textDataArray.length; i++) { textDataArray[i] = +textDataArray[i]; }
			console.log('sendTextToGraph called.');
			console.log('barChartData set to ', text);
			this.props.setState({}, 'barChartData', textDataArray)
		}
	}
	componentDidMount() {
		this.loadCommentsFromServer();
		setInterval(this.loadCommentsFromServer, this.props.pollInterval);
	}

	render() {
		return (
			<div style={ style.commentBox }>
				<h2 style={ style.title }></h2>
				<CommentList 
				onCommentDelete={ this.handleCommentDelete }
				textToGraph = { this.sendTextToGraph }
				onCommentUpdate={ this.handleCommentUpdate }
				data={ this.state.data }/>
				<CommentForm onCommentSubmit={ this.handleCommentSubmit }/>
			</div>
		)
	}
}
export default CommentBox;