//CommentList.js
import React, { Component } from 'react';
import Comment from './comment';
import style from './style';
// MongoDB adds an ID tag to each post labeled “_id”. 
class CommentList extends Component {

	constructor(props) { 
		super(props);
	}
	render() {
		let commentNodes = this.props.data.map(comment => {
			return (
				<Comment 
					author={ comment.author } 
					uniqueID={ comment['_id'] }
					onCommentDelete={ this.props.onCommentDelete }
					textToGraph={ this.props.textToGraph }
					onCommentUpdate={ this.props.onCommentUpdate }
					key={ comment['_id'] } >
					{ comment.text }
				</Comment>
			)
		})
		return (
			<div style={ style.commentList }>
				{ commentNodes }
			</div>
		)
	}
}
export default CommentList;