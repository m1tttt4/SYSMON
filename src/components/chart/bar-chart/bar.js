import React, { PropTypes } from 'react';
import * as d3 from 'd3';
import Rect from './rect';

const Bar = React.createClass({
	

	// getInitialState: function() {
		// console.log('Bar', this)
		// return {
		// 	...this.props
		// };
	// },
	// shouldComponentUpdate: function(nextProps) {
		
	// 	return this.props !== nextProps
	// },
	componentDidUpdate: function() {
		// console.log('Bar updated')
	},
	render() {
		const props = this.props;
		const data = props.barChartData.map(function(d) {
			
			return d;
		});
		
		const yScale = d3.scaleLinear()
			.domain([0, d3.max(data)])
			.range([0, this.props.height]);
		
		const xScale = d3.scaleBand()
			.domain(d3.range(this.props.barChartData.length))
			.rangeRound([0, this.props.width], 0.1);
	
		const bars = data.map(function(point, i) {
			const height = yScale(point),
			y = props.height - height,
			width = xScale.bandwidth(),
			x = xScale(i);
			
		
			return (
				<Rect height={height} 
					width={width} 
					x={x} 
					y={y} 
					key={i} 
				/>
			)
		});

		return (
			<g>{bars}</g>
		);
	}
}); 

export default Bar;   