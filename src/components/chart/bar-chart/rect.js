import React, { PropTypes } from 'react';
import * as d3 from 'd3';
// import ease from 'd3-ease';



const SetIntervalMixin = {
	componentWillMount: function() {
		this.intervals = [];
	},
	setInterval: function() {
		this.intervals.push(setInterval.apply(null, arguments));
	},
	componentWillUnmount: function() {
		this.intervals.map(clearInterval);
	}
};


const Rect = React.createClass({
	mixins: [SetIntervalMixin],
	getDefaultProps: function() {
		return {
			width: 0,
			height: 0,
			x: 0,
			y: 0
		}
	},
	// propTypes: {
	//     width: number.isRequired,
	//     height: number.isRequired,
	//     x: number.isRequired,
	//     y: number.isRequired,
	//     fill: string.isRequired,
	//     data: oneOfType([
	//         array,
	//         object
	//     ]).isRequired,
	//     onMouseEnter: func,
	//     onMouseLeave: func
	// },
	getInitialState: function() {
		return {
			milliseconds: 0,
			height: 0
		};
	},
	// shouldComponentUpdate: function(nextProps) {
	// 	// console.log('Rect should Update')
		
	// 	return this.props.height !== this.state.height;
	// },

	componentWillMount: function() {
		console.log('Rect will mount');
	},

	componentWillReceiveProps: function(nextProps) {
		this.setState({milliseconds: 0, height: 0});
	},

	componentDidMount: function() {
	
		this.setInterval(this.tick, 10);
	},

	tick: function(start) {
		this.setState({milliseconds: this.state.milliseconds + 10});
	},

	render() {
		// const easyeasy = d3.ease('back-out');
		
		const height = this.state.height + 
			(this.props.height - this.state.height) * d3.easeCubic(Math.min(1, this.state.milliseconds/1000));
		const y = this.props.height - height + this.props.y;
		return (
			<rect className="bar"
			height={height} 
			y={y} 
			width={this.props.width}
			x={this.props.x}
			onMouseOver={(evt) => {
				// console.log('windowWidth', window.innerWidth, 'testValueA', testValueA, 'xscale', props.xScale(coords[0]))
				// console.log(evt.target.cx.animVal.value, evt.target.cy.animVal.value)
				evt.target.setAttribute('stroke', 'white')
			}}
			onMouseOut={(evt) => {
				evt.target.setAttribute('stroke', '')
			}}>
			</rect>
		);
	}

	// render() {
	// 	const {
	// 		x,
	// 		y,
	// 		width,
	// 		height,
	// 		fill,
	// 		data,
	// 		onMouseEnter,
	// 		onMouseLeave
	// 	} = this.props;

	// 	return <rect
	// 		className="bar"
	// 		x={x}
	// 		y={y}
	// 		width={width}
	// 		height={height}
	// 		fill={fill}
	// 		onMouseMove={e => onMouseEnter(e, data)}
	// 		onMouseLeave={e => onMouseLeave(e)}
	// 	/>;
	// }
});

export default Rect;