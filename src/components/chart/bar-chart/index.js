import React from 'react'; 
import Bar from './bar';

const plotStyle = {
	width   : window.innerWidth*.48,
	height  : window.innerHeight*.75,
	padding : 20,
	color : 'purple',
	textAlign : 'left'
};

class BarChart extends React.Component {  
	constructor(props) {
		console.log('BarChart super', super(props)) 
		super(props);
		console.log('BarChart Props: ', this.props)
		console.log('BarChart State: ', this.state)

	};
	
	shouldComponentUpdate(nextProps) {
		
		return this.props !== nextProps;
	};
	render() {
		return (
			 <svg width={this.props.width} height={this.props.height} >
				<Bar 								
					{...this.props}
				/>
				
			</svg> 
		);
	}
};

export default BarChart;
