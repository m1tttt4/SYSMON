import React from 'react';

const renderCircles = (props) => {
	return (coords, index) => {
		let circleProps = {}
		let testValueA = (props.width - props.padding)
		switch(props.xScale(coords[0])) {
				case testValueA:
					circleProps = {
						cx: props.xScale(coords[0]),
						cy: props.yScale(coords[1]),
						r: 10,
						key: index,
						fill: 'black'
					}
					break;
				case 200:
					circleProps = {
						cx: props.xScale(coords[0]),
						cy: props.yScale(coords[1]),
						r: 10,
						key: index,
						fill: 'pink'
					}
						break;
				default: 
					circleProps = { 
						cx: props.xScale(coords[0]),
						cy: props.yScale(coords[1]),
						r: 8,
						key: index,
						fill: '#947057'
					}
		}
		return <circle 
			onMouseOver={(evt) => {
					// console.log('windowWidth', window.innerWidth, 'testValueA', testValueA, 'xscale', props.xScale(coords[0]))
					// console.log(evt.target.cx.animVal.value, evt.target.cy.animVal.value)
					evt.target.setAttribute('r', '10'), 
					evt.target.setAttribute('stroke', 'blue')

			}} 
			onMouseOut={(evt) => {
				evt.target.setAttribute('r', '10'),
				evt.target.setAttribute('cx', props.xScale(coords[0])),
				evt.target.setAttribute('cy', props.yScale(coords[1]))
			}}
			{...circleProps}> 
				Values 
			</circle>;
	};
};

export default (props) => {
	return <g>
		{ props.scatterPlotData.map(renderCircles(props)) }
	</g>
}
