import React       from 'react';
import ScatterPlot from './scatter-plot/';
import ChartText 	 from './chart-text';
import BarChart from './bar-chart/';
import Bar from './bar-chart/bar';
const headerStyle = {
	width   : window.innerWidth,
	height  : '100%',
	padding : 0,
	color : 'purple',
	textAlign: 'center'
};
const chartStyle = { 
	height: 'auto',
	width: 'auto',
	position: 'relative',
	marginLeft: 'auto',
	marginRight: 'auto',
	backgroundColor: '#669899',
	backgroundRepeat: 'no-repeat',
	color:'pink',
	overflow: 'hidden',
	textAlign : 'left'
};

const plotStyle = {
	width   : window.innerWidth*.48,
	height  : window.innerHeight*.20,
	padding : 20,
	color : 'purple',
	textAlign : 'left'
};

const textStyle = {
	fontSize: '1px',
	color: 'white'
}

// some basic data
const all = [1,2,3,4,5,6,7,8,9,10,11,12,13];
const filtered = [5,4,3,2,1];

const Chart = React.createClass({
	componentDidMount: function() {
		console.log('Chart - index.js mounted')
		console.log('Chart props: ', this.props.chartState.barChartData)
		console.log('Chart state: ', this.state.barChartData)
	},
	getDefaultProps: function() {
		return {
			width: 0,
			height: 0,
			barChartData: []
		}
	},
	// shouldComponentUpdate: function(nextProps) {
	// 	console.log('Chart should update');
	// 	return this.props.chartState.barChartData !== nextProps.chartState.barChartData;
	// },
	componentDidUpdate: function(){
		console.log('chart updated');
	},
	getInitialState: function() {
		return {
			...this.props.chartState
		}
	},
	showAll: function() {
		this.setState({barChartData : this.props.chartState.barChartData})
	},

	filter: function() {
		this.setState({barChartData: all});
	},
	
	
	render() {
		return (
			<div>
				<div style={headerStyle}>
					<h1>NOC</h1>
					<div className="selection">
						<ul>
							<p onClick={this.showAll}>All</p>
							<p onClick={this.filter}>Filter</p>
						</ul>
					</div>
		
					<div style={chartStyle}>
						<BarChart 
							{...plotStyle}
							{...this.props.chartState}>
						</BarChart>
						<ScatterPlot {...this.props.chartState} {...plotStyle} />
						<ChartText {...this.props.chartState} />
					</div>
				</div>
			</div>
		)
	}
})

export default Chart;
