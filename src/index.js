//Importing local scripts and React
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './reducers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import 'bootstrap/dist/css/bootstrap.min.css';
import SpreadsheetContainer from './containers/SpreadsheetContainer';

const store = configureStore();

//Rendering router
render(
	<MuiThemeProvider>
		<Provider store={store}>
			<Router>
				<Route path="/" render={() => <SpreadsheetContainer/>} ></Route>
			</Router>
		</Provider>
	</MuiThemeProvider>	
	, document.getElementById('app')

)
