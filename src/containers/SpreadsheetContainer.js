import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/spreadsheetActions';
import spreadsheetState from '../reducers/spreadsheetState.js'
// import styles from './styles.module.css';
import ColumnSelector from '../components/spreadsheet/ColumnSelector.js';

class SpreadsheetContainer extends Component {
	//declare propTypes for Chart
	// static propTypes = {
	// 	actions: PropTypes.object.isRequired,
	// 	spreadsheetState: PropTypes.object.isRequired
	// };
	//pass specific actions and chart state into Chart
	render() {
		return ( 
			<div style={{height: '100%'}}>	
				<ColumnSelector 
					setState={this.props.actions.setState}
					setStates={this.props.actions.setStates}
					appendState={this.props.actions.append}
				/>
			</div>			
		);
	}
}
function mapStateToProps(state) {
	return {
		//mapping state.chartState to chartState
		spreadsheetState: state.spreadsheetState
	};
}
function mapDispatchToProps(dispatch) {
	return {
		//mapping dispatch to actions
		actions: bindActionCreators(actions, dispatch)
	};
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SpreadsheetContainer);