import { combineReducers } from 'redux';
import spreadsheetState from './spreadsheetState';
import dashboardState from './dashboardState';

const rootReducer = combineReducers({
	spreadsheetState,
	dashboardState
});

export default rootReducer;