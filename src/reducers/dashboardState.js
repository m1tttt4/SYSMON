import {SET_STATE, SET_STATES, APPEND, SEARCH} from '../constants/ActionTypes';
import { createStore, applyMidddleware } from 'redux';
import thunk from 'redux-thunk';
import objectAssign from 'object-assign';

const initialState = {
    screenWidth: typeof window === 'object' ? window.innerWidth : null,
    dataKey: 'servers'
};
export default function dashboardState(state = initialState, action) {
	switch (action.type) {
		case SET_STATE:			
			let newState = objectAssign({}, state);
			newState[action.fieldName] = action.value;
			return newState;
		case SET_STATES:	
		{		
			let newState = objectAssign({}, state);
			for (let i in action.fieldNames) {
				newState[action.fieldNames[i]] = action.values[i];
			}
			return newState;
		}
		case APPEND:
		{
			let newState = objectAssign({}, state);
			newState[action.fieldName] += (action.value);
			return newState;
		}
		case SEARCH:
		{
			let newState = objectAssign({}, state);
			newState[action.fieldName] = action.value;
			return newState;
		}
	}
	return state;
	
}
