import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';

export default function configureStore(initialState) {
	let store;
	if (window.devToolsExtension) {
		//Look up what create store does and what thunk does
		store = window.__REDUX_DEVTOOLS_EXTENSION__()(createStore)(rootReducer, initialState, applyMiddleware(thunk));
	} else {
		store = createStore(rootReducer, initialState, applyMiddleware(thunk));
	}
	//if environment is hot- add hmr
	if (module.hot) {
		module.hot.accept('../reducers', () => {
			const nextReducer = require('../reducers/index').default;
			store.replaceReducer(nextReducer);
		});
	}

	return store;
}
