import {SET_STATE, SET_STATES, APPEND, SEARCH} from '../constants/ActionTypes';
import { createStore, applyMidddleware } from 'redux';
import thunk from 'redux-thunk';
import objectAssign from 'object-assign';
// The number of data points for the chart.
const numDataPoints = 4;
// A function that returns a random number from 0 to 1000
const randomNum     = () => Math.floor(Math.random() * 10);
const scalar = () => {
	let scalarArray = []
	for (var i=0; i<numDataPoints/2; i++) {
		for (var j=0; j<numDataPoints/2; j++) {
			scalarArray.push([i,j])
		}
	}
	return scalarArray;
}
// A function that creates an array of numDataPoints elements of (x, y) coordinates.
const scalarDataSet = () => {
	return Array.apply(null, scalar());
}
const initialState = {
	logWidth: 0,
	activeColumnHeaders: [],
	// allColumnHeaders: []
}
export default function spreadsheetState(state = initialState, action) {
	switch (action.type) {
		case SET_STATE:			
			let newState = objectAssign({}, state);
			newState[action.fieldName] = action.value;
			return newState;
		case SET_STATES:	
		{		
			let newState = objectAssign({}, state);
			for (let i in action.fieldNames) {
				newState[action.fieldNames[i]] = action.values[i];
			}
			return newState;
		}
		case APPEND:
		{
			let newState = objectAssign({}, state);
			newState[action.fieldName] += (action.value);
			return newState;
		}
		case SEARCH:
		{
			let newState = objectAssign({}, state);
			newState[action.fieldName] = action.value;
			return newState;
		}
	}
	return state;
}