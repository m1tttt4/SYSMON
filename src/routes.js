import React from 'react';
import { Route } from 'react-router-dom';
import SpreadsheetContainer from './containers/SpreadsheetContainer';


export default (<div>
	    <Route path="/" component={SpreadsheetContainer} ></Route>
    </div>
)
