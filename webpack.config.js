import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
const DEV = 'development' ;
const PROD = 'production';

//Plugins based on running environment
const getPlugins = function (env) {
  console.log(JSON.stringify(env));
  const GLOBALS = {
    'process.env.NODE_ENV': JSON.stringify(env)
  };
  const plugins = [];
  switch (env) {
    case PROD:
      console.log('Pushing Production Plugins');
      plugins.push(new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }));
      plugins.push(new CompressionPlugin());
      plugins.push(new MiniCssExtractPlugin());
      plugins.push(new HtmlWebpackPlugin({
        template: 'src/index.ejs',
        favicon: 'src/images/favicon.ico',
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true
        },
        inject: true
        }));
      break;
    case DEV:
      console.log('Pushing Development Plugins');
      plugins.push(new webpack.DefinePlugin({"global.GENTLY": false}));
      plugins.push(new webpack.HotModuleReplacementPlugin());
      plugins.push(new webpack.NoEmitOnErrorsPlugin());
      break;
  }
  return plugins;
};
const getEntry = function (env) {
  const entry = [];
  if (env === DEV ) { // only want hot reloading when in dev.
    console.log('Pushing Development Entry for webpack-hot-middleware');
    entry.push('webpack-hot-middleware/client?reload=true');
  }
  entry.push(path.resolve(__dirname, 'src/index'));
  return entry;
};
const getLoaders = function (env) {
  const loaders =
  [{
    test: /(\.jsx|\.js)$/,
    include: [path.join(__dirname, 'src'), path.join(__dirname, 'devTools')],
    exclude: [path.join(__dirname, '/node_modules/')],
    use: ['babel-loader']
  },
  {
    test: /(\.log)$/,
    use: 'raw-loader'
  },
  {
    test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
    include: [path.join(__dirname, 'node_modules')],
    use: 'url-loader?limit=100000&name=[name].[ext]'
  },
  {
    test: /\.css$/,
    use: [
      {
              loader: 'style-loader'
            },
            {
                loader: "css-loader",
                options: {
                    sourceMap: true,
                    modules: true
        }
            }]
  }];
  if (env === PROD) {
    console.log('Pushing Production Loaders');
  }
  else {
    console.log('Pushing Development Loaders');
    loaders.push({test: /\.(jpe?g|png|gif|svg)$/i, loaders: ['url-loader?limit=8192', 'img-loader']});
  }
  return loaders;
};
function getConfig(env) {
  console.log("Env = " + env)
  return {
    context: __dirname,
    devtool: env === PROD ? 'hidden-source-map' : 'cheap-module-source-map',
    entry: getEntry(env),
    target: env === PROD ? 'web' : 'web',
    output: {
      path: __dirname + '/dist', // Output of build
      publicPath: '/',
      filename: env === PROD ? 'sysmon-[name].[chunkhash].js' : 'sysmon-dev.bundle.js',
    },
    resolve: {
      extensions: ['.jsx', '.scss', '.js', '.json', '.node'],
      modules: [
        'node_modules',
        path.resolve(__dirname, 'node_modules')
      ]
    },
    plugins: getPlugins(env),
    module: {
      rules: getLoaders(env)
    },
    mode: env
  };
}
export default getConfig;
