'use strict'
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();
const static_path = path.join('static');
const dynamic_path = path.join('dist');
const port = process.env.API_PORT || 8090;

//configures the API to use bodyParser and look for 
//JSON data in the request body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Credentials', 'true');
	res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Content-Encoding');
  //and remove caching so we get the most recent data
	res.setHeader('Cache-Control', 'no-cache');
	next();
});

//Sets the route path & initializes the API
router.get('/', function(req, res) {
	res.json({ message: 'API Initialized!'});
});
app.get('*.js', function (req, res, next) {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.set('Content-Type', 'text/javascript');
		res.set('Cache-Control', 'public, max-age=3600000')
        next();
});
app.get('*.map', function (req, res, next) {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.set('Content-Type', 'text/javascript');
        next();
});


app.use('/static', express.static(static_path, { maxAge: '10d' }), router)
app.use(express.static(dynamic_path, { maxAge: '10d' }), router)	
	.get('./', function (req, res) {
		res.sendFile('dist/index.html', { root: dynamic_path });
	});

//starts the server and listens for requests
app.listen(port, function() {
	console.log(`Production database server running on port ${port}`);
});
