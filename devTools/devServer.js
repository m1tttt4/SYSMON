// This file configures the development web server
// which supports hot reloading and synchronized testing.
// Require Browsersync along with webpack and middleware for it
import browserSync from 'browser-sync';
// Required for react-router browserHistory
// see https://github.com/BrowserSync/browser-sync/issues/204#issuecomment-102623643
import historyApiFallback from 'connect-history-api-fallback';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfigBuilder from '../webpack.config';

const webpackConfig = webpackConfigBuilder('development');
const bundler = webpack(webpackConfig);

// Run Browsersync and use middleware for Hot Module Replacement
browserSync({
	
	server: {
		baseDir: 'src',
		middleware: [
			historyApiFallback(),
			webpackDevMiddleware(bundler, {
				// Dev middleware can't access config, so we provide publicPath
				publicPath: webpackConfig.output.publicPath,
				// pretty colored output
				stats: { colors: true },
				serverSideRender: false,
				// Set to false to display a list of each file that is being bundled.
				noInfo: false,

				proxy: {
					'*': {
						target: process.env.DEVSERVER,
						changeOrigin: true,
						secure: false,
						cookieDomainRewrite: '',
						onProxyReq: function (request, req, res) {
							request.setHeader('origin', process.env.DEVSERVER)
						}
					},
				}
			}),
			// bundler should be the same as above
			webpackHotMiddleware(bundler)			
		]
	},
	port: 9080,
	ui: {
			// domain: 'localhost',
			port: 9090,
			weinre: {
					port: 9091
			}
	},
	socket: {
		domain: process.env.DEVSERVER
	},

	// no need to watch '*.js' here, webpack will take care of it for us,
	// including full page reloads if HMR won't work
	files: [
		'src/*.html'
	]
});


