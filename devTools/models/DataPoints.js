'use strict';
import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var datapointsSchema = new Schema({
	key: String,
	dataKey: String,
	text: String,
	date: Date,
});
var DataPoints = mongoose.model('DataPoints', datapointsSchema, 'datapoints');
module.exports = DataPoints;