//model/comments.js
import mongoose from 'mongoose';

const Schema = mongoose.Schema;
//create new instance of the mongoose.schema. the schema takes an 
//object that shows the shape of database entries.
const commentsSchema = new Schema({
 author: String,
 text: String,
 date: Date,
 key: String,
 environ: String
});

const Comment = mongoose.model('Comment', commentsSchema, 'comments');

export default Comment;
