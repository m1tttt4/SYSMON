import express from 'express';
import http from 'http';
import request from 'request';
import io from 'socket.io';
import mongoose from 'mongoose';
import Comment from './models/Comment.js';
import DataPoints from './models/DataPoints.js';

const mongodb = 'mongodb://' + process.env.MONGOSERVER + '/sysmon';
console.log(process.env);
try {
	mongoose.connect(mongodb, { useNewUrlParser: true });
	console.log('Connected to: ', mongodb);
}
catch(err) {
	console.log('Unable to connect to mongo server on 27017/sysmon')
	console.log('Is mongod running?')
}

let fileServer = express()

fileServer.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Credentials', 'true');
	res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  //	res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");  
	//and remove caching so we get the most recent comments
	res.setHeader('Cache-Control', 'no-cache');
	next();
});

let fileAccessor = http.Server(fileServer);
let fileSocket = io(fileAccessor);

fileServer.get('/', function(req, res){
	res.sendFile('/home/m4tt4ew/gitlab/SYSMON/static/index.html');
});
function formatDate(date) {
	var monthNames = [
		"Jan", "Feb", "Mar",
		"Apr", "May", "Jun", "Jul",
		"Aug", "Sep", "Oct",
		"Nov", "Dec"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();
	var hour = date.getHours();
	var minute = date.getMinutes();
	var seconds = date.getSeconds();

	return monthNames[monthIndex]+' '+day+' '+year+' '+hour+':'+minute+':'+seconds;
}

let emitting = {}
fileSocket.on('connection', function(socket){
	console.log('a user connected');
	socket.on('subscribeToTimer', (interval) => {
		console.log('Socket subscribeToTimer - interval:', interval);
		
		setInterval(() => {
			socket.emit('timer', formatDate(new Date()));
		}, interval);
	});

    socket.on('subscribeToComments', (key, interval) => {
      Comment.find({environ: key}).exec(function(err, comments) {
        if (err) throw err;
        console.log("Request: "+key
               +"\nFrom socket: "+socket.client.conn.id
               +"\nFound comments: "+comments.length
        )
        socket.emit(key, {comments: comments})
      })
    });

	socket.on('saveComment', (comment) => {
		console.log('Socket saveComment - comment:', comment);
		// console.log(comment)
		var modelComment = new Comment({
			author: comment.author,
			text: comment.text,
			date: comment.date,
			key: comment.key,
			environ: comment.environ
		})

		modelComment.save(function(err) {
			if (err) throw err;
			console.log('Comment saved successfully');
			socket.emit('commented', comment)
		})
	});
	socket.on('deleteComment', (commentId) => {
		console.log('Socket deleteComment - commentId:', commentId);
	
		Comment.remove( {_id: commentId}, function(err) {
			if (err) throw err;
			console.log('Comment deleted successfully');
			socket.emit('comment_deleted', commentId)
		})
	});
	socket.on('disconnect', function(){
		console.log('user disconnected');
	});
});

fileAccessor.listen(process.env.PROXYPORT, process.env.PROXYHOST, function(){
	console.log('Comment proxy port: ', process.env.PROXYPORT);
});
